package com.tiger.microservice.ecommerce.Controller;

import com.tiger.microservice.ecommerce.Assembler.ProductAssembler;
import com.tiger.microservice.ecommerce.Entity.EcommerceProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EcommerceController {

	@Autowired
	ProductAssembler productAssembler;

	@GetMapping("/ecommerceProducts")
	public EcommerceProductResponse getAllEcommerceProducts(){
		return new EcommerceProductResponse(productAssembler.getEcommerceProducts());
	}
}
